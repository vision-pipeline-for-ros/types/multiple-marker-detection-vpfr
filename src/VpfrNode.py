#!/usr/bin/env python3

"""
multiple-marker-detection
Copyright 2020 Vision Pipeline for ROS
Author: Sebastian Bergner <sebastianbergner5892@gmail.com>
SPDX-License-Identifier: MIT
"""

import sys
from sensor_msgs.msg import Image
from vpfrmultiplemarkerdetection.msg import SingleMarker
from vpfrmultiplemarkerdetection.msg import MultipleMarker
from vpfr import *


if __name__ == "__main__":

    if len(sys.argv) < 4:
        print("usage: VpfrNode.py subscribechannel publishchannel additionalArgs")
    else:
        VisionPipeline(
            "vpfrmultiplemarkerdetection",
            sys.argv[1],
            Image,
            sys.argv[2],
            MultipleMarker,
            sys.argv,
        )
