# Multiple marker detection VPFR
![alt text](https://gitlab.com/vision-pipeline-for-ros/vision-pipeline-for-ros/-/raw/master/icons/logo_long.svg)



# Vision Pipeline

[VPFR](https://gitlab.com/vision-pipeline-for-ros/vision-pipeline-for-ros)

[Wiki](https://gitlab.com/vision-pipeline-for-ros/vision-pipeline-for-ros/wikis/home)

---

# Multiple marker detection VPFR
This is a type of vision pipeline.  
This type starts an instance of a vision pipeline.  
The instance then loads all algorithms that match the type.  
This type processes images and searches for markers on the images.  
When all markers have been found, it is written in the output topic.
 


Details on this in the [wiki](https://gitlab.com/vision-pipeline-for-ros/types/multiple-marker-detection-vpfr/-/wikis/home)